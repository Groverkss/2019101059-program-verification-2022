
/- Remove the placeholder and complete the following proofs -/
open classical

variables A B C : Prop
variables p q r : Prop

-- 1
section
  example : A ∨ B → B ∨ A :=
    λ h : A ∨ B,
    have h1 : A → B ∨ A, from (
        λ ha : A,
        show B ∨ A,
          from or.inr ha),
    have h2 : B → B ∨ A, from (
        λ hb : B,
        show B ∨ A,
        from or.inl hb),
    show B ∨ A, 
    from or.elim h h1 h2
end

-- 2
section
  example : A ∧ (B ∨ C) → (A ∧ B) ∨ (A ∧ C) :=
  λ h : A ∧ (B ∨ C),
  have a : A,
    from h.left,
  have boc : (B ∨ C),
    from h.right,
  have br : B → (A ∧ B) ∨ (A ∧ C), from (
    λ b : B,
    have anb : A ∧ B,
      from and.intro a b,
    show (A ∧ B) ∨ (A ∧ C),
    from or.inl anb),
  have cr : C → (A ∧ B) ∨ (A ∧ C), from (
    λ c : C,
    have anc : A ∧ C,
      from and.intro a c,
    show (A ∧ B) ∨ (A ∧ C),
    from or.inr anc),
  show (A ∧ B) ∨ (A ∧ C), 
  from boc.elim br cr
end

-- 3
section
  lemma not_not {a : Prop}: ¬¬a → a :=
  λ hnn, by_contradiction (λ hn, absurd hn hnn)

  lemma not_not_op {a : Prop} (ha : a) : ¬¬a :=
    λ hna : ¬a, absurd ha hna

  example : ¬(A ∨ B) ↔ ¬A ∧ ¬B :=
  have leftimpl : ¬(A ∨ B) → ¬A ∧ ¬B,
    from λ h : ¬(A ∨ B),
    have hnia : A → false,
      from λ ha: A,
      have hab : A ∨ B,
        from or.inl ha,
      show false,
      from absurd hab h,
    have hna : ¬A,
      from by_contradiction (not_not_op hnia),
    have hnib : B → false,
      from λ hb: B,
      have hab : A ∨ B,
        from or.inr hb,
      show false,
      from absurd hab h,
    have hnb : ¬B,
      from by_contradiction (not_not_op hnib),
    show ¬A ∧ ¬B,
    from and.intro hna hnb,
  have rightimpl : ¬A ∧ ¬B → ¬(A ∨ B), 
    from λ h : ¬A ∧ ¬B,
    have af : A → false,
      from not.intro h.left,
    have bf : B → false,
      from not.intro h.right,
    show ¬(A ∨ B),
    from by_contradiction
      (λ nnhab : ¬¬(A ∨ B),
      have hab : (A ∨ B),
        from not_not nnhab,
      show false,
      from or.elim hab af bf),
  show ¬(A ∨ B) ↔ ¬A ∧ ¬B,
  from iff.intro leftimpl rightimpl
end

-- 4
section
  example : ((p → q) ∧ (¬r → ¬q)) → (p → r) :=
  λ h : (p → q) ∧ (¬r → ¬q),
  show p → r, from
    λ hp : p,
    show r,
    from by_contradiction (
      λ hnr : ¬r,
      have hq1 : q, from h.left hp,
      have hq2 : ¬q, from h.right hnr,
      show false,
      from absurd hq1 hq2)
end