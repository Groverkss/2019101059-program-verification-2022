/- The syntax of propositional logic expressions is defined here in lean (along with a few helpful operators) -/

namespace hidden

structure var :=
mk :: (idx : ℕ) 

-- Partial variable. Holds either a variable or a boolean value.
inductive partial
| pval : bool → partial
| pvar : var → partial

inductive Expr
| atom : partial -> Expr
| not  : Expr -> Expr
| and  : Expr -> Expr -> Expr
| or   : Expr -> Expr -> Expr
| impl : Expr -> Expr -> Expr

prefix `atom` : 49 := Expr.atom
prefix `¬`    : 50 := Expr.not
infix `∧`     : 51 := Expr.and
infix `∨`     : 52 := Expr.or
infix `⇒`     : 53 := Expr.impl

/- Q1: This question is a warm-up to using propositions inductively. Define a function `size` that returns the size of the propositional logic expression -/
-- def size : Expr -> ℕ

def size : Expr → ℕ
| (atom v) := 1
| (¬ e) := (size e) + 1
| (e1 ∧ e2) := (size e1) + (size e2) + 1
| (e1 ∨ e2) := (size e1) + (size e2) + 1
| (e1 ⇒ e2) := (size e1) + (size e2) + 1

/- Q2. Now we get into a bit of an open area. Remember the definitions of an `interpretation` and a `valuation` that we did in class.

How would you build propositional logic syntax and semantics in lean, such that you can have a function `valuation : Expr x Interpretation -> bool` - that takes a type `Expr` - your type for propositional expressions ; a type `Interpretation`, and returns a boolean?

(Note: your `valuation` function does not need to be exactly this type! What this question asks of you is to implement propositional logic syntax and semantics in a way that supports a mechanism for defining expressions, defining interpretations and computing valuations on the expressions x interpretations)  

To see what "building propositional logic" means, you could refer to the propositional logic syntax I built in [the Propositions In Lean tutorial notes](../../tutorial-notes/propositions-in-lean.org). -/

-- Boolean operators.
def bnot : bool → bool
| tt := ff
| ff := tt

def band : bool → bool → bool
| tt tt := tt
| _ _ := ff

def bor : bool → bool → bool
| ff ff := ff
| _ _ := tt

def bimpl : bool → bool → bool
| tt ff := ff
| _ _ := tt

-- Partial valuations over each operator.
def pnot : Expr → Expr
| (atom (partial.pval v)) := atom (partial.pval (bnot v))
| e := ¬ e

def pand : Expr → Expr → Expr
| (atom (partial.pval v1)) (atom (partial.pval v2)) := (atom (partial.pval (band v1 v2)))
| e1 e2 := e1 ∧ e2

def por : Expr → Expr → Expr
| (atom (partial.pval v1)) (atom (partial.pval v2)) := (atom (partial.pval (bor v1 v2)))
| e1 e2 := e1 ∨ e2

def pimpl : Expr → Expr → Expr
| (atom (partial.pval v1)) (atom (partial.pval v2)) := (atom (partial.pval (bimpl v1 v2)))
| e1 e2 := e1 ⇒ e2

-- Value for a single variable.
structure interpretn :=
mk :: (k : var) (v : bool)

-- Interpretation over a list of variables.
def Interpretation : Type := list interpretn

-- Find the value of `v` in the interpretation `intr`. 
-- Return `none` if no value found.
def find_val : Interpretation → var → option bool
| [] v := none
| (a::l) v := if (a.k.idx = v.idx) then a.v else (find_val l v)

-- Create a partial variable given a optional bool.
-- Return a value if the bool has some value.
-- Return a variable if the bool has no value.
def get_val : option bool → var → Expr
| none v := atom (partial.pvar v)
| (some b) _ := atom (partial.pval b)

-- Partial valuation over an expression given an interpretation.
def valuation : Expr → Interpretation → Expr
| (atom (partial.pval v)) _ := (atom (partial.pval v))
| (atom (partial.pvar v)) intr := get_val (find_val intr v) v
| (¬ v) intr := pnot (valuation v intr)
| (a ∧ b) intr := pand (valuation a intr) (valuation b intr)
| (a ∨ b) intr := por (valuation a intr) (valuation b intr)
| (a ⇒ b) intr := pimpl (valuation a intr) (valuation b intr)

/- Q3. Can you implement a type `is_sat (e : Expr)? Implement it as a record type (remember `vector` from the class notes) -/

-- Join two given optional variables. Just get one if
-- any one is available.
def join_var : ℕ → ℕ → ℕ
| 0 0 := 0
| a 0 := a
| 0 b := b
| a b := a

-- Try to get a variable from an Expr.
-- The Expr is required to have atleast one variable.
-- If no variable is found, a variable with idx 0 is returned.
def get_var : Expr → ℕ
| (atom (partial.pvar v)) := v.idx
| (atom (partial.pval v)) := 0 -- Techincally this should return an error
| (¬ e) := join_var (get_var e) 0
| (e1 ∧ e2) := join_var (get_var e1) (get_var e2)
| (e1 ∨ e2) := join_var (get_var e1) (get_var e2)
| (e1 ⇒ e2) := join_var (get_var e1) (get_var e2)

def create_interp (idx : ℕ) (b : bool) : Interpretation :=
[(interpretn.mk (var.mk idx) b)]

def valuate_single (e : Expr) (b : bool) : Expr :=
valuation e (create_interp (get_var e) b)

-- TODO: Prove that it is well founded :/
def is_sat : Expr → bool
| (atom (partial.pval v)) := v
| e := bor (is_sat (valuate_single e ff)) (is_sat (valuate_single e tt)) 

-- Testing
def cpart : ℕ → partial :=
λ x, partial.pvar (var.mk x)

def P := atom cpart 1 
def Q := atom cpart 2 
def R := atom cpart 3 

def i1 := interpretn.mk (var.mk 1) tt
def i2 := interpretn.mk (var.mk 2) ff
def i3 := interpretn.mk (var.mk 3) tt

def inter : Interpretation := [i1, i2]

def test1 := (P ∧ Q ∧ R) ⇒ P

-- #reduce (valuation test1 inter)

def test2 := (P ∧ Q) ∨ (Q ∧ R) ∨ (P ∧ R) ∨ (¬ Q)

-- #reduce test2

def test3 := (P ∧ Q) ∧ ¬(P ∧ Q)

-- #eval is_sat test3

def test4 := (¬(P ∧ Q)) ∧ (P ∧ Q)

-- #eval is_sat test4

end hidden